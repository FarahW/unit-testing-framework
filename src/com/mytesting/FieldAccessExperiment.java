package com.mytesting;

import java.lang.reflect.Field;

public class FieldAccessExperiment {

    public static void main(String[] args) {
        ClassForTesting instance = new ClassForTesting();

        // Accessing the public field 'myDouble' from class ClassForTesting
        try {
            Field myDoubleField = instance.getClass().getField("myDouble");
            System.out.println("Public Field (myDouble) before: " + myDoubleField.getDouble(instance));

            // Modify the public field
            myDoubleField.setDouble(instance, 3.14);
            System.out.println("Public Field (myDouble) after: " + myDoubleField.getDouble(instance));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        // Accessing the private field 'myString'
        try {
            Field myStringField = instance.getClass().getDeclaredField("myString");
            myStringField.setAccessible(true);
            System.out.println("Private Field (myString) before: " + myStringField.get(instance));

            // Modify the private field
            myStringField.set(instance, "Reflection is powerful!");
            System.out.println("Private Field (myString) after: " + myStringField.get(instance));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
