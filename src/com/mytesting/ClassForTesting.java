package com.mytesting;


public class ClassForTesting {
    
    // A public field of type double
    public double myDouble = 1.23;
    
    // A private field of type String
    private String myString = "This is a secret string";
    
    // A public method that prints the value of myDouble
    public void printMyDouble() {
        System.out.println("Value of myDouble: " + myDouble);
    }
    
    // A private method that prints the value of myString
    private void printMyString() {
        System.out.println("Value of myString: " + myString);
    }
}
