package com.mytesting;

import java.util.logging.Logger;
import java.util.logging.Level;

public class ClassForTestingTest {

    private MyUnitTestFramework testFramework = new MyUnitTestFramework();
    private static final Logger LOGGER = Logger.getLogger(ClassForTestingTest.class.getName());
    
    public void testMyDoubleField() {
        ClassForTesting instance = new ClassForTesting();
        
        // Testing myDouble from ClassForTesting
        double myDoubleValue = 3.14;
        instance.myDouble = myDoubleValue; 
        
        testFramework.assertEqual(instance.myDouble, myDoubleValue, "testMyDoubleField");
    }
    
    public void testException() {
        ClassForTesting instance = new ClassForTesting();
        
        // This method should throw an exception
        testFramework.assertEqual(instance.myDouble, null, "testException");
    }
    
    public void testMyDoubleNotEqual() {
        ClassForTesting instance = new ClassForTesting();
        double differentValue = 2.34;
        testFramework.assertNotEqual(instance.myDouble, differentValue, "testMyDoubleNotEqual");
    }

    public void testTrueCondition() {
        testFramework.assertTrue(2 < 3, "testTrueCondition");
    }

    public void testFalseCondition() {
        testFramework.assertFalse(3 < 2, "testFalseCondition");
    }

    public static void main(String[] args) {
        ClassForTestingTest testInstance = new ClassForTestingTest();
        LOGGER.log(Level.INFO, "Running test suite: ClassForTestingTest");
        testInstance.testFramework.runTests(testInstance);
    }
}
