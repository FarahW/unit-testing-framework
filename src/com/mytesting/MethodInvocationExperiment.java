package com.mytesting;
import java.lang.reflect.Method;

public class MethodInvocationExperiment {

    public static void main(String[] args) {
        ClassForTesting instance = new ClassForTesting();

        // Invoking the public method 'printMyDouble' from class ClassForTesting
        try {
            Method printMyDoubleMethod = instance.getClass().getMethod("printMyDouble");
            printMyDoubleMethod.invoke(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Invoking the private method 'printMyString'
        try {
            Method printMyStringMethod = instance.getClass().getDeclaredMethod("printMyString");
            printMyStringMethod.setAccessible(true);
            printMyStringMethod.invoke(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
