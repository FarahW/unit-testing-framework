package com.mytesting;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.SimpleFormatter;


public class MyUnitTestFramework {
	private static final Logger LOGGER = Logger.getLogger(MyUnitTestFramework.class.getName());
    private int successfulTests = 0;
    private int failedTests = 0;
    
    static {
        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.ALL);
        consoleHandler.setFormatter(new SimpleFormatter());
        LOGGER.addHandler(consoleHandler);
        LOGGER.setLevel(Level.ALL);
    }


    public void assertEqual(Object actual, Object expected, String testName) {
        if ((actual == null && expected == null) || (actual != null && actual.equals(expected))) {
            successfulTests++;
            LOGGER.log(Level.INFO, testName + " PASSED");
        } else {
            failedTests++;
            LOGGER.log(Level.WARNING, testName + " FAILED - Expected: " + expected + ", but got: " + actual);
        }
    }

    public void assertNotEqual(Object actual, Object expected, String testName) {
        if (actual != null && !actual.equals(expected)) {
            successfulTests++;
            System.out.println(testName + " PASSED");
        } else {
            failedTests++;
            System.out.println(testName + " FAILED - Expected not equal to: " + expected + ", but got: " + actual);
        }
    }

    public void assertTrue(boolean condition, String testName) {
        if (condition) {
            successfulTests++;
            System.out.println(testName + " PASSED");
        } else {
            failedTests++;
            System.out.println(testName + " FAILED - Expected true but got false");
        }
    }

    public void assertFalse(boolean condition, String testName) {
        if (!condition) {
            successfulTests++;
            System.out.println(testName + " PASSED");
        } else {
            failedTests++;
            System.out.println(testName + " FAILED - Expected false but got true");
        }
    }

    public void runTests(Object testInstance) {
        Class<?> testClass = testInstance.getClass();
        Method[] methods = testClass.getDeclaredMethods();

        for (Method method : methods) {
            if (method.getName().startsWith("test")) {
                try {
                    method.invoke(testInstance);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        printTestResults();
    }

    private void printTestResults() {
        System.out.println("Successful Tests: " + successfulTests);
        System.out.println("Failed Tests: " + failedTests);
    }
}
